/**
 * @module parseHostname
 * @desc Parses the hostname (`window.location.hostname` or your own supplied
 *   string) and returns an object with the hostname (kinda redundant), the
 *   simple domain and the media name. Used to find the correct API endpoint or
 *   to find the correct logo for the masthead.
 * @version 1.0
 *
 * @example
 * import {parseHostname} from '@ta-interaktiv/parse-hostname';
 *
 * let parsedHost = parseHostname();
 * // Returns on http://interaktiv.tagesanzeiger.ch/2016/example-project
 * // {
 * //    projectDomain: interaktiv.tagesanzeiger.ch,
 * //    publicationDomain: tagesanzeiger.ch,
 * //    publicationName: tagesanzeiger
 * // }
 */

/**
 * @typedef {Object} Hostname
 * @property {string} projectDomain Full host name, like
 * "interaktiv.tagesanzeiger.ch".
 * @property {string} publicationDomain Last two parts of the project
 * domain, like "tagesanzeiger.ch".
 * @property {string} publicationName Just the media name, like "tagesanzeiger".
 */

/**
 * @param {string} [host=window.location.hostname] A
 * host name like `interaktiv.tagesanzeiger.ch`, in case it differs
 * from the current location.
 *
 * @returns {Hostname}
 */
export default function parseHostname (host = window.location.hostname) {
  // If the url contains slashes, we might be dealing with a full-on URL
  if (/[/:]/.test(host)) {
    host = new window.URL(host).hostname
  }

  // get the full host name. Replace with 'interaktiv.tagesanzeiger.ch' if
  // developing on localhost.
  let projectDomain = /localhost$/.test(host)
    ? 'interaktiv.tagesanzeiger.ch'
    : host

  /** Create an array with the projectDomain parts */
  let hostnameArray = projectDomain.split('.')

  /**
   * Domain without any sub domains = last two parts of the projectDomain
   */
  let publicationDomain = hostnameArray.slice(-2).join('.')

  /**
   * Media Name: Domain without top level domain.
   */
  let publicationName = hostnameArray[hostnameArray.length - 2]

  return {
    projectDomain: projectDomain,
    publicationDomain: publicationDomain,
    publicationName: publicationName
  }
}
