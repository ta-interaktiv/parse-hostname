<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/ta-interactive/parse-hostname/compare/1.0.0...1.1.0) (2018-02-05)


### Features

* only set placeholder domain for pure localhost domains ([1b0eade](https://gitlab.com/ta-interactive/parse-hostname/commit/1b0eade))
* publish code as ES module ([d1af8cb](https://gitlab.com/ta-interactive/parse-hostname/commit/d1af8cb)), closes [#5](https://gitlab.com/ta-interactive/parse-hostname/issues/5)



# Changelog

## Upcoming
- Standalone version of parse-hostname.
- Added test cases for the module.
