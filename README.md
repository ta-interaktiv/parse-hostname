# Parse Hostname
Parse a host name to figure out from where a visitor is visiting.

## Installation

```bash
yarn add @ta-interaktiv/parse-hostname
```

<a name="module_parseHostname"></a>

## parseHostname
Parses the hostname (`window.location.hostname` or your own supplied
  string) and returns an object with the hostname (kinda redundant), the
  simple domain and the media name. Used to find the correct API endpoint or
  to find the correct logo for the masthead.

**Version**: 1.0  
**Example**  
```js
import {parseHostname} from '@ta-interaktiv/parse-hostname';

let parsedHost = parseHostname();
// Returns on http://interaktiv.tagesanzeiger.ch/2016/example-project
// {
//    projectDomain: interaktiv.tagesanzeiger.ch,
//    publicationDomain: tagesanzeiger.ch,
//    publicationName: tagesanzeiger
// }
```

* [parseHostname](#module_parseHostname)
    * [module.exports([host])](#exp_module_parseHostname--module.exports) ⇒ <code>Hostname</code> ⏏
        * [~Hostname](#module_parseHostname--module.exports..Hostname) : <code>Object</code>

<a name="exp_module_parseHostname--module.exports"></a>

### module.exports([host]) ⇒ <code>Hostname</code> ⏏
**Kind**: Exported function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [host] | <code>string</code> | <code>&quot;window.location.hostname&quot;</code> | A host name like `interaktiv.tagesanzeiger.ch`, in case it differs from the current location. |

<a name="module_parseHostname--module.exports..Hostname"></a>

#### module.exports~Hostname : <code>Object</code>
**Kind**: inner typedef of [<code>module.exports</code>](#exp_module_parseHostname--module.exports)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| projectDomain | <code>string</code> | Full host name, like "interaktiv.tagesanzeiger.ch". |
| publicationDomain | <code>string</code> | Last two parts of the project domain, like "tagesanzeiger.ch". |
| publicationName | <code>string</code> | Just the media name, like "tagesanzeiger". |



## Contributing

The specifications of the module are in the `test` directory and code can be tested against by running

```bash
yarn run test
```


[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)


