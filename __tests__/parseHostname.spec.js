import {parseHostname} from '../src/index'

describe('Parse Hostname Function', function () {

  describe('Basic parsing, passing in URL directly', () => {
    it('should react correctly to interaktiv domain', () => {
      const parsed = parseHostname('https://interaktiv.derbund.ch/2018/test')

      expect(parsed.projectDomain).toEqual('interaktiv.derbund.ch')
      expect(parsed.publicationDomain).toEqual('derbund.ch')
      expect(parsed.publicationName).toEqual('derbund')
    })

    it('should correctly parse a non-interaktiv domain', () => {
      const parsed = parseHostname('https://www.24heures.ch/extern/interactive_wch/2018/ssr-check/')

      expect(parsed.projectDomain).toEqual('www.24heures.ch')
      expect(parsed.publicationDomain).toEqual('24heures.ch')
      expect(parsed.publicationName).toEqual('24heures')
    })

    it('should correctly work on localhost', () => {
      const parsed = parseHostname('http://localhost:8000')

      expect(parsed.projectDomain).toEqual('interaktiv.tagesanzeiger.ch')
      expect(parsed.publicationDomain).toEqual('tagesanzeiger.ch')
      expect(parsed.publicationName).toEqual('tagesanzeiger')
    })

    it('should work on a local development domain', () => {
      const parsed = parseHostname('http://localhost.tdg.ch/index.html')

      // Don't fall back to local host version
      expect(parsed.projectDomain).toEqual('localhost.tdg.ch')
      expect(parsed.publicationDomain).toEqual('tdg.ch')
      expect(parsed.publicationName).toEqual('tdg')

      // Make sure it doesn't fall back to local host rules
      expect(parsed.publicationName).not.toEqual('tagesanzeiger')
      expect(parsed.projectDomain).not.toEqual('interaktiv.tagesanzeiger.ch')
    })
  })

  describe('Parsing functionality using window.location', () => {

    it('Should get the project domain from Tagesanzeiger', () => {
      expect(parseHostname().projectDomain)
        .toEqual('interaktiv.tagesanzeiger.ch')
    })

    it('Should get the publication domain from Tagesanzeiger', () => {

      expect(parseHostname().publicationDomain)
        .toEqual('tagesanzeiger.ch')
    })

    it('Should get the publication name from Tagesanzeiger', () => {
      expect(parseHostname().publicationName)
        .toEqual('tagesanzeiger')
    })

    afterEach(function () {

    })
  })

  describe('Manual parsing using a parameter', function () {
    it('Should get the project domain from Der Bund', () => {
      expect(parseHostname('interaktiv.derbund.ch').projectDomain)
        .toEqual('interaktiv.derbund.ch')
    })
    it('Should get the publication domain from Der Bund', () => {
      expect(parseHostname('interaktiv.derbund.ch').publicationDomain)
        .toEqual('derbund.ch')
    })
    it('Should get the publication name from Der Bund', () => {
      expect(parseHostname('interaktiv.derbund.ch').publicationName)
        .toEqual('derbund')
    })
    it('Should do something sensible when the sub domain is not available',
      () => {

        expect(parseHostname('tagesanzeiger.ch').projectDomain)
          .toEqual('tagesanzeiger.ch')
      })
  })

  describe('Replacement when using localhost during development', function () {
    beforeEach(function () {
      jsdom.reconfigure({url: 'http://localhost:9000'})
    })

    it('Should replace localhost with interaktiv.tagesanzeiger.ch', function () {
      expect(parseHostname().projectDomain).toEqual('interaktiv.tagesanzeiger.ch')
    })

    it('Should replace localhost with interaktiv.tagesanzeiger.ch' +
      ' publication domain',
      function () {
        expect(parseHostname().publicationDomain)
          .toEqual('tagesanzeiger.ch')
      })
  })

})


