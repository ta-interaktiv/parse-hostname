/**
 * @jest-environment node
 */

import { parseHostname } from '../src'

describe('Throwing', function () {

  it('Should throw an error when window.location is not defined', () => {
    expect(() => parseHostname().projectDomain).toThrow()
  })
})
